package data;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.xuggler.ICodec;

public class VideoGenerator {


    private static final String outputFilename = "testout.mp4";

    private static Dimension screenBounds;

    private static Map<String, File> imageMap = new HashMap<String, File>();

    public static void main(String[] args) {

        final IMediaWriter writer = ToolFactory.makeWriter(outputFilename);

        screenBounds = Toolkit.getDefaultToolkit().getScreenSize();

        writer.addVideoStream(0, 0, ICodec.ID.CODEC_ID_MPEG4,screenBounds.width / 2, screenBounds.height / 2);
        writer.addAudioStream(1,0, 2, 44100);

        File folder = new File("video");
        File[] listOfFiles = folder.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".jpg");//add proper extension list
            }
        });

        int indexVal = 0;
        for (File file : listOfFiles) {
            if (file.isFile()) {
                indexVal++;
                System.out.println("file.getName() :"+file.getName());
                imageMap.put(file.getName(), file);
            }
        }

        //for (int index = 0; index < SECONDS_TO_RUN_FOR * FRAME_RATE; index++) {
        int cumulative_time=0;
        for (int index = 1; index <= listOfFiles.length; index++) {
            BufferedImage screen = getImage(index);
            BufferedImage bgrScreen = convertToType(screen, BufferedImage.TYPE_3BYTE_BGR);
            writer.encodeVideo(0, bgrScreen, 5000*(index-1), TimeUnit.MILLISECONDS);
            if(index==listOfFiles.length){
            	writer.encodeVideo(0, bgrScreen, 5000*(index)-500, TimeUnit.MILLISECONDS);
            }
            	
        }
        writer.flush();
        // tell the writer to close and write the trailer if needed
        writer.close();
        System.out.println("Video Created");

    }

    public static BufferedImage convertToType(BufferedImage sourceImage, int targetType) {
        BufferedImage image;
        if (sourceImage.getType() == targetType) {
            image = sourceImage;
        }
        else {
            image = new BufferedImage(sourceImage.getWidth(),
            sourceImage.getHeight(), targetType);
            image.getGraphics().drawImage(sourceImage, 0, 0, null);
        }
        return image;
    }

    private static BufferedImage getImage(int index) {

        try {
            String fileName=index+".jpg";
            System.out.println("fileName :" + fileName);
            File img = imageMap.get(fileName);

            BufferedImage in=null;
            if (img != null) {
                System.out.println("img :"+img.getName());
                in = ImageIO.read(img);
            }else
            {
                System.out.println("++++++++++++++++++++++++++++++++++++++index :" + index);
                img = imageMap.get(1);
                in = ImageIO.read(img);
            }
            return in;

        }

        catch (Exception e) {

            e.printStackTrace();

            return null;

        }

    }

}